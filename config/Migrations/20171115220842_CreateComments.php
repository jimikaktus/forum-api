<?php
use Migrations\AbstractMigration;

class CreateComments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('comments');
        $table->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('post_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('comment', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addForeignKey('post_id', 'posts', ['id'],
            ['constraint'=>'fk_post_id', 'delete' => 'CASCADE', 'update' => 'NO_ACTION']
        );
        $table->addForeignKey('user_id', 'users', ['id'],
            ['constraint'=>'fk_user_id', 'delete' => 'CASCADE', 'update' => 'NO_ACTION']
        );
        $table->create();
    }
}
