<?php
use Migrations\AbstractMigration;

class Users extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('users');
        $table
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false
            ])
            ->addColumn('surname', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false
            ])
            ->addColumn('password', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false
            ])
            ->addColumn('token', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('token_created', 'datetime', [
                'default' => null,
                'null' => true,
            ])
            ->create();
    }
    public function down()
    {
        $this->dropTable('users');
    }
}
