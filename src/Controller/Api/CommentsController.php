<?php
namespace App\Controller\Api;
use App\Controller\AppController;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 */
class CommentsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadModel('Comments');
    }    
    
    public function add()
    {        
        $newComment = $this->Comments->newEntity($this->request->getData());
        $newComment->user_id = $this->Auth->user('id');
        if ($this->Comments->save($newComment)) {
            $message = 'Saved';
            $result = $newComment;
        } else {
            $message = 'Error';
            $result = $newComment->errors();
        }
        $this->set([
            'message' => $message,
            'result' => $result,
            '_serialize' => ['message', 'result']
        ]);   
    }
}