<?php
namespace App\Controller\Api;
use App\Controller\AppController;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 */
class PostsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadModel('Posts');
    }    
    
    public function add()
    {        
        $newPost = $this->Posts->newEntity($this->request->getData());
        $newPost->user_id = $this->Auth->user('id');
        if ($this->Posts->save($newPost)) {
            $message = 'Saved';
            $result = $newPost;
        } else {
            $message = 'Error';
            $result = $newPost->errors();
        }
        $this->set([
            'message' => $message,
            'result' => $result,
            '_serialize' => ['message', 'result']
        ]);   
    }
    
    public function showList()
    {
        $posts = $this->Posts->find('allByLatest');
        $this->set([
            'posts' => $posts,
            '_serialize' => ['posts']
        ]);
    }
    
    public function show($id = null)
    {
        if (empty($id)) {
            $this->set([
                'message' => 'Error',
                'content' => 'Id cannot be empty',
                '_serialize' => ['message', 'content']
            ]);
        }
        else {
            $post = $this->Posts->find('oneWithComments', ['id' => $id]);
            $this->set([
                'post' => $post,
                '_serialize' => ['post']
            ]);
        }
    }
    
    
    public function delete($id = null)
    {
        $post = $this->Posts->findByIdAndUserId($id, $this->Auth->user('id'))->first();
        $message = 'Error';
        if ($post && $this->Posts->delete($post)) {
            $message = 'Deleted';
        }
        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
    }
}