<?php
namespace App\Controller\Api;
use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow(['login', 'register']);
        $this->loadModel('Users');
    }
    
    public function login()
    {
	$user = $this->Auth->identify($this->request,$this->response);
	$this->set(compact('user'));
	$this->set('_serialize',['user']);
    }
    
    public function register()
    {
        $newUser = $this->Users->newEntity($this->request->getData());
        if ($this->Users->save($newUser)) {
            $message = 'Saved';
            $result = $newUser;
        } else {
            $message = 'Error';
            $result = $newUser->errors();
        }
        $this->set([
            'message' => $message,
            'result' => $result,
            '_serialize' => ['message', 'result']
        ]);    
    }
}